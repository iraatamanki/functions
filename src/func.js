const getSum = (str1, str2) => {
  if (typeof(str1) != 'string' || typeof(str2) != 'string' || str1.match(/[A-Z]/gi) || str2.match(/[A-Z]/gi)){
    return false;
  }
  str1 = str1.length === 0 ?  0 : parseInt(str1);
  str2 = str2.length === 0 ?  0 : parseInt(str2);
  return (str1 + str2).toString();
}

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let posts = 0, comm = 0;
  for (const post of listOfPosts){
    if(post.author === authorName){
      posts += 1;
    }
    if (post.comments?.length > 0){
      for (const comment of post.comments){
        if(comment.author === authorName){
          comm += 1;
        }
      }
    }
  }
  return `Post:${posts},comments:${comm}`;
}

const tickets=(people)=> {
  let cash = 0; 
  for (const person of people){
    if (person > 25){
      cash -= person - 25;
    }
    if (cash < 0){
      return 'NO'
    }
    cash += person; 
  }
  return 'YES';
}


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
